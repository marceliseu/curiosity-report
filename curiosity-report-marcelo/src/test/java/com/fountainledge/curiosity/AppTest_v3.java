package com.fountainledge.curiosity;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


import org.junit.*;

/**
 * Unit test for simple App.
 */
public class AppTest_v3 
    extends TestCase
{
	

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest_v3( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest_v3.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
     
}
