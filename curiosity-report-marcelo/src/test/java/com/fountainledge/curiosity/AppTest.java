package com.fountainledge.curiosity;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import org.junit.Test;


class ShortFormatter extends Formatter {
    // this method is called for every log records
    public String format(LogRecord rec) {
    		String msg ;
    		msg = calcDate(rec.getMillis()) + " " + 
    		rec.getSourceMethodName() + ":" + 
    		rec.getMessage() + "\n";;
    		//msg = rec.getMessage() + "\n";
		return msg;
    }
    private String calcDate(long millisecs) {
        //SimpleDateFormat date_format = new SimpleDateFormat("dd MMM yyyy HH:mm");
   	 SimpleDateFormat date_format = new SimpleDateFormat("dd/MM/yy HH:mm");
       Date resultdate = new Date(millisecs);
       return date_format.format(resultdate);
   }
}

public class AppTest {
	
	private final static Logger logger = Logger.getLogger(AppTest.class.getName());
	static private Formatter formatter  = new ShortFormatter();
	
	public AppTest() {
		Logger rootLog = Logger.getLogger("");
		rootLog.setLevel( Level.INFO );
		Handler rootHandler = rootLog.getHandlers()[0];
		rootHandler.setLevel( Level.INFO );   // Default console handler
		rootHandler.setFormatter(formatter);
		
		logger.info("Inicializado " );
	}

	@Test
	public void TestaDateFormat()
	{
		Timestamp timestamp = Timestamp.valueOf("2019-08-01 00:00:00");
	    String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
		

	    
		DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE; 
		String timestampAsString2 = formatter.format(timestamp.toLocalDateTime());
		System.out.println(timestampAsString2);
		
        String pattern = "yyyy-MM-dd";   //2018-11-01
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
 	   	String timestampAsString = sdf.format(timestamp);
 	   	assertEquals("2019-08-01", timestampAsString);
 	   
        System.out.println("timestampAsString="+timestampAsString);
        System.out.println("timestamp="  + timestamp.toString());
        
        
        
        /*
         *           
            String pattern = "yyyy-MM-dd";   //2018-11-01
     	   	String timestampAsString = new SimpleDateFormat(pattern).format(timestamp);  
            System.out.println("timestampAsString="+timestampAsString);
            System.out.println("timestamp="  + timestamp.toString());
            System.out.println("timestamp="  + timestamp.toLocaleString());
         */

	}
	

}
