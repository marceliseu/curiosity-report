package com.fountainledge.curiosity.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Cameras {
	private String name;
	private String full_name;

	public String getName() {
	return name;
	}

	public void setName(String name) {
	this.name = name;
	}

	public String getFull_name() {
	return full_name;
	}

	public void setFull_name(String full_name) {
	this.full_name = full_name;
	}
}
