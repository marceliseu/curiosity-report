package com.fountainledge.curiosity.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Photo {



private Integer id;
private Integer sol;
private Camera camera;
private String img_src;
private String earth_date;
private Rover rover;

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public Integer getSol() {
return sol;
}

public void setSol(Integer sol) {
this.sol = sol;
}

public Camera getCamera() {
return camera;
}

public void setCamera(Camera camera) {
this.camera = camera;
}

public String getImg_src() {
return img_src;
}

public void setImg_src(String img_src) {
this.img_src = img_src;
}

public String getEarth_date() {
return earth_date;
}

public void setEarth_date(String earth_date) {
this.earth_date = earth_date;
}

public Rover getRover() {
return rover;
}

public void setRover(Rover rover) {
this.rover = rover;
}	
	
}
