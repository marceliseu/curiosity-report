package com.fountainledge.curiosity.model;

import java.util.List;

import javax.annotation.Resource;

@Resource
public class MarsReport {
    
    private WeatherReport weatherReport;
    private List<String> marsImages;
    private String test ;
    
    public WeatherReport getWeatherReport() {
        return weatherReport;
    }

    public void setWeatherReport(WeatherReport weatherReport) {
        this.weatherReport = weatherReport;
    }

	public List<String> getMarsImages() {
		return marsImages;
	}

	public void setMarsImages(List<String> marsImages) {
		this.marsImages = marsImages;
	}

	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}


	 

}
