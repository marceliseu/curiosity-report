package com.fountainledge.curiosity.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Photos {

	private List<Photo> photos = null;

	public List<Photo> getPhotos() {
	return photos;
	}

	public void setPhotos(List<Photo> photos) {
	this.photos = photos;
	}

}
