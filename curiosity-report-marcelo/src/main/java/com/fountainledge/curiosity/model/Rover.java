
package com.fountainledge.curiosity.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Rover {

private Integer id;
private String name;
private String landing_date;
private String launch_date;
private String status;
private Integer max_sol;
private String max_date;
private Integer total_photos;
private List<Cameras> cameras = null;

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getLanding_date() {
return landing_date;
}

public void setLanding_date(String landing_date) {
this.landing_date = landing_date;
}

public String getLaunch_date() {
return launch_date;
}

public void setLaunch_date(String launch_date) {
this.launch_date = launch_date;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public Integer getMax_sol() {
return max_sol;
}

public void setMax_sol(Integer max_sol) {
this.max_sol = max_sol;
}

public String getMax_date() {
return max_date;
}

public void setMax_date(String max_date) {
this.max_date = max_date;
}

public Integer getTotal_photos() {
return total_photos;
}

public void setTotal_photos(Integer total_photos) {
this.total_photos = total_photos;
}

public List<Cameras> getCameras() {
return cameras;
}

public void setCameras(List<Cameras> cameras) {
this.cameras = cameras;
}

}