package com.fountainledge.curiosity.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fountainledge.curiosity.model.MarsReport;
import com.fountainledge.curiosity.model.MarsWeatherFeed;
import com.fountainledge.curiosity.model.WeatherReport;

@Service
public class CuriosityServiceImpl implements CuriosityService {
    
    @Autowired
    protected NASAService nasaService;
    
    public MarsReport getLatestMarsReport() {       
        MarsReport marsReport = new MarsReport();
        List<WeatherReport> latestWeatherReports = this.getLatestWeatherReports();
        if (latestWeatherReports.size() > 0) {
            marsReport.setWeatherReport(latestWeatherReports.get(0));
            WeatherReport weatherreport = latestWeatherReports.get(0);
            Timestamp timestamp = weatherreport.getTerrestrial_date();
            
            System.out.println("getTerrestrial_date="+weatherreport.getTerrestrial_date().toString());
            System.out.println("getEarthTime="+weatherreport.getEarthTime().toString());
            String pattern = "yyyy-MM-dd";    
     	   	String timestampAsString = new SimpleDateFormat(pattern).format(timestamp);  
     	    marsReport.setMarsImages(nasaService.getMarsImages(timestampAsString));             
        }
        String dt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date());  
        marsReport.setTest(dt + " Hello Buddy !!!!");   
        return marsReport;
    }
    
    public MarsReport getMarsReportforDate(String date) {        
        MarsReport marsReport = new MarsReport();
        List<WeatherReport> weatherReports = this.getWeatherReportsforDate(date);
        if (weatherReports.size() > 0) {
            marsReport.setWeatherReport(weatherReports.get(0));
     	    marsReport.setMarsImages(nasaService.getMarsImages(date));             
        }
        String dt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date());  
        marsReport.setTest(dt + " Hello Buddy !!!!");   
        return marsReport;
    }    

    public List<WeatherReport> getLatestWeatherReports() {
        MarsWeatherFeed marsWeatherFeed = nasaService.getMarsWeatherFeed();
        return marsWeatherFeed.getResults();
    }
    
    public List<WeatherReport> getWeatherReportsforDate(String date) {
        MarsWeatherFeed marsWeatherFeed = nasaService.getMarsWeatherFeed(date);
        return marsWeatherFeed.getResults();
    }
    

        
}
