package com.fountainledge.curiosity.service;

import java.util.List;

import com.fountainledge.curiosity.model.MarsWeatherFeed;

public interface NASAService {
    
    public MarsWeatherFeed getMarsWeatherFeed();
    
    public MarsWeatherFeed getMarsWeatherFeed(String date);
    
    public List<String> getMarsImages(String earth_date);
    
}
