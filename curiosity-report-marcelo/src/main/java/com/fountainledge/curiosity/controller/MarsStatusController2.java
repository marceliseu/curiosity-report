package com.fountainledge.curiosity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.fountainledge.curiosity.model.MarsReport;
import com.fountainledge.curiosity.service.CuriosityService;

@CrossOrigin
@Controller
public class MarsStatusController2 {
    
    @Autowired
    CuriosityService curiosityService;  

    @GetMapping(path = "/marsreport2")
    public String getMarsReport2(@RequestParam(required = false) String earth_date, Model model) {
    	MarsReport masreport = null;
    	if (earth_date != null) {
    		masreport = curiosityService.getMarsReportforDate(earth_date);
    	} else {
    		masreport = curiosityService.getLatestMarsReport();
    	}
    	model.addAttribute("masreport", masreport);	
        return "marsreport2";
    }      

}