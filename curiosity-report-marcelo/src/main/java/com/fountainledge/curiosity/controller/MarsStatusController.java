package com.fountainledge.curiosity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.fountainledge.curiosity.model.MarsReport;
import com.fountainledge.curiosity.service.CuriosityService;

@CrossOrigin
@RestController
public class MarsStatusController {
    
    @Autowired
    CuriosityService curiosityService;

    @GetMapping(path = "/api/mars/report", produces = "application/json; charset=UTF-8")    
    public MarsReport getMarsReport() {
        return curiosityService.getLatestMarsReport();
    }
    
    @GetMapping(path = "/api/mars/report/{date}", produces = "application/json; charset=UTF-8")
	public MarsReport lerPost(@PathVariable String date) {
    	//System.out.println(date);
		return curiosityService.getMarsReportforDate(date);
	}     
     

}