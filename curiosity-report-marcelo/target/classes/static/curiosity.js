
txt = "";
img_link = "<a target=\"_blank\" href=\"link\"><img src=\"link\"  style=\"width:150px\"></a> ";

function getMarsReport(earth_date) {
	var url_api = "";
	if (earth_date === undefined || earth_date === null) {
		url_api = "http://localhost:8080/api/mars/report/" ;
	} else {
		url_api = "http://localhost:8080/api/mars/report/" + earth_date ;
	}
	console.log("url=" + url_api );
    
	$.ajax({
		method : "GET",
		url : url_api
	}).done(
			function(data) {
				marsStatus = data;
				document.getElementById("weather-atmo-opacity").textContent = marsStatus.weatherReport.atmo_opacity;
				document.getElementById("weather-max-temp").textContent = marsStatus.weatherReport.max_temp;
				document.getElementById("weather-min-temp").textContent = marsStatus.weatherReport.min_temp;
				document.getElementById("weather-wind-speed").textContent = marsStatus.weatherReport.wind_speed;
				document.getElementById("weather-terrestrial-date").textContent = marsStatus.weatherReport.earthTime;
				
				console.log(marsStatus.test);
				var links = marsStatus.marsImages;				
				links.forEach(linkToImage);			
				document.getElementById("divmars").innerHTML = txt;

			}).fail(
					function(data) {
						alert("Could not retrieve Mars report." + data);
					});

}

function linkToImage(value, index, array) {
	  var img_link_new = img_link.replace(/link/g,value);
	  txt = txt + img_link_new;
}

$(document).ready(function() {

	var urlParams = new URLSearchParams(window.location.search);
	var parm_exits = urlParams.has('earth_date');
	if (parm_exits) {
		var earth_date= urlParams.get('earth_date');
		getMarsReport(earth_date);
	} else {
		getMarsReport();		
	}
	


});
