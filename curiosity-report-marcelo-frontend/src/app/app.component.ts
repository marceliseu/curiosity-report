import { Component} from '@angular/core';
import { WeatherReport } from "./marsreport/WeatherReport.model";
import { Marsreport } from "./marsreport/marsreport.model";
import { MarsreportService } from './marsreport/marsreport.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  {
  title = 'curiosity-report-marcelo-frontend';

  ngOnInit() {
  }

  constructor(
    public marsreportService: MarsreportService
  ){ }

  


}
