export class WeatherReport {
    terrestrial_date: Date;
    min_temp: number;
    max_temp: number;
    pressure: number;
    pressure_string: string;
    wind_speed: number;
    atmo_opacity: string;
    sunrise: Date;
    sunset: Date;

  
    constructor() {

    }

}
