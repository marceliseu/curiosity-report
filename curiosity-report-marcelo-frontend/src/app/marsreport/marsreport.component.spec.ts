import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarsreportComponent } from './marsreport.component';

describe('MarsreportComponent', () => {
  let component: MarsreportComponent;
  let fixture: ComponentFixture<MarsreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarsreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarsreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
