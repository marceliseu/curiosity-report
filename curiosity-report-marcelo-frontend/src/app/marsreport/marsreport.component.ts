import { Component, OnInit , Input, HostBinding} from '@angular/core';
import { WeatherReport } from "./WeatherReport.model";
import { Marsreport } from "./marsreport.model";
import { MarsreportService } from './marsreport.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NgbDateFRParserFormatter } from "./ngb-date-fr-parser-formatter"
import { NgbDatepickerConfig, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-marsreport',
  templateUrl: './marsreport.component.html',
  styleUrls: ['./marsreport.component.css'],
  providers: [{provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter}]
})
export class MarsreportComponent implements OnInit {
  marsreport: Marsreport;
  earth_date_model;
  searchForm: FormGroup;

  constructor(private marsreportService: MarsreportService, private formBuilder: FormBuilder,) {
    this.createSearchForm();
   }

  ngOnInit() {
  }

  onSubmit() {
    if (this.searchForm.value.optdate == "latestdate")
      this.marsreportService.GetLatestMarsReport().subscribe(data => this.marsreport = data);
    else 
    {
      /*
      console.log("this.searchForm.value.optdate="+ this.searchForm.value.optdate);
      console.log("this.searchForm.value.day length="+ this.earth_date_model.day.length);
      console.log("this.searchForm.value.earth_date="+ this.searchForm.value.earth_date.day);
      console.log("this.searchForm.value.earth_date="+ this.searchForm.value.earth_date.month);
      console.log("this.searchForm.value.earth_date="+ this.searchForm.value.earth_date.year);
      */
      var txt1 = ("0" + this.searchForm.value.earth_date.day).slice(-2);
      var txt2 = ("0" + this.searchForm.value.earth_date.month).slice(-2);
      var txt3 =  txt3 = this.searchForm.value.earth_date.year;
      // change to format yyyy-mm-dd
      let earth_date_str = txt3 + "-" + txt2 + "-" + txt1;
      this.marsreportService.GetMarsReportforDate(earth_date_str).subscribe(data => this.marsreport = data);
    }
}

  createSearchForm(){
    this.searchForm = this.formBuilder.group({
      optdate: [''],  
      earth_date: ['']
    });
  }

  getLatestMarsReport1(): void {
    this.marsreportService.GetLatestMarsReport().subscribe(data => this.marsreport = data);
   }

   getLatestMarsReport(): void {
    this.marsreportService.GetLatestMarsReport().subscribe(data => { 
      this.marsreport = data;
      console.log('getLatestMarsReport');
      console.log('test=' + this.marsreport.test);
    });
   }

  getMarsReportforDate(earth_date): void {
    this.marsreportService.GetMarsReportforDate(earth_date).subscribe(data => {
      this.marsreport = data;
      console.log('getMarsReportforDate earth_date=' + earth_date );
      console.log('test=' + this.marsreport.test);
    })
  }    

}
