import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Marsreport } from './marsreport.model';

@Injectable({
  providedIn: 'root'
})
export class MarsreportService {

   // Base url
   baseurl = 'http://localhost:8080';


   constructor(private http: HttpClient) { }


  // Http Headers
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

   // Get specific Marsreport
   GetMarsReportforDate(earth_date): Observable<Marsreport> {
    return this.http.get<Marsreport>(this.baseurl + '/api/mars/report/' + earth_date)
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    )
  }

    // Get latest Marsreport
    GetLatestMarsReport(): Observable<Marsreport> {
      return this.http.get<Marsreport>(this.baseurl + '/api/mars/report')
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      )
    }

     // Error handling
   errorHandl(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
 }

 
}
