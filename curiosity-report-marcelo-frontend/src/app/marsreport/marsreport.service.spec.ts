import { TestBed } from '@angular/core/testing';

import { MarsreportService } from './marsreport.service';

describe('MarsreportService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MarsreportService = TestBed.get(MarsreportService);
    expect(service).toBeTruthy();
  });
});
