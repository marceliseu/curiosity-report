import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MarsreportComponent }   from './marsreport/marsreport.component';


const routes: Routes = [
  { path: 'marsreport', component: MarsreportComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
